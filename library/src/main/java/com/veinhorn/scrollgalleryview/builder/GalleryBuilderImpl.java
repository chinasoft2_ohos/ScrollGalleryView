package com.veinhorn.scrollgalleryview.builder;

import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.ScrollGalleryView;
import java.util.ArrayList;
import java.util.List;

/**
 * GalleryBuilderImpl
 *
 * @since 2021-05-13
 */
public class GalleryBuilderImpl implements GalleryBuilder {
    private ScrollGalleryView mGalleryView;
    private GallerySettings mSettings;

    private ScrollGalleryView.OnImageClickListener mOnImageClickListener;
    private ScrollGalleryView.OnImageLongClickListener mOnImageLongClickListener;

    private List<MediaInfo> mMedias;

    /**
     * GalleryBuilderImpl
     *
     * @param galleryView
     */
    public GalleryBuilderImpl(ScrollGalleryView galleryView) {
        this.mGalleryView = galleryView;
        this.mMedias = new ArrayList<>();
    }

    /**
     * settings
     *
     * @param settings contains options for gallery
     * @return GalleryBuilder
     */
    @Override
    public GalleryBuilder settings(GallerySettings settings) {
        this.mSettings = settings;
        return this;
    }

    /**
     * onImageClickListener
     *
     * @param onImageClickListener
     * @return GalleryBuilder
     */
    @Override
    public GalleryBuilder onImageClickListener(ScrollGalleryView.OnImageClickListener onImageClickListener) {
        this.mOnImageClickListener = onImageClickListener;
        return this;
    }

    /**
     * onImageLongClickListener
     *
     * @param onImageLongClickListener
     * @return GalleryBuilder
     */
    @Override
    public GalleryBuilder onImageLongClickListener(
            ScrollGalleryView.OnImageLongClickListener onImageLongClickListener) {
        this.mOnImageLongClickListener = onImageLongClickListener;
        return this;
    }

    /**
     * add
     *
     * @param media MediaInfo
     * @return GalleryBuilder
     */
    @Override
    public GalleryBuilder add(MediaInfo media) {
        this.mMedias.add(media);
        return this;
    }

    /**
     * add
     *
     * @param medias List
     * @return GalleryBuilder
     */
    @Override
    public GalleryBuilder add(List<MediaInfo> medias) {
        this.mMedias.addAll(medias);
        return this;
    }

    /**
     * build
     *
     * @return ScrollGalleryView
     */
    @Override
    public ScrollGalleryView build() {
        // check here all parameters
        return mGalleryView
                .setThumbnailSize(mSettings.getThumbnailSize())
                .setZoom(mSettings.isZoomEnabled())
                .addOnImageClickListener(mOnImageClickListener)
                .addOnImageLongClickListener(mOnImageLongClickListener)
                .setFragmentManager(mMedias)
                .addMedia(mMedias);
    }
}
