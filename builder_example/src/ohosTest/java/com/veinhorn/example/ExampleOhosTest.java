/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.veinhorn.example;

import com.veinhorn.scrollgalleryview.ScrollGalleryView;
import com.veinhorn.scrollgalleryview.builder.GalleryBuilder;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * ExampleOhosTest
 *
 * @since 2021-05-13
 */
public class ExampleOhosTest {
    /**
     * testBundleName
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.veinhorn.scrollgalleryview.example.builder", actualBundleName);
    }

    /**
     * testScrollGalleryView
     */
    @Test
    public void testScrollGalleryView() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().getContext();
        ScrollGalleryView scrollGalleryView = new ScrollGalleryView(context,null);
        assertNotNull(scrollGalleryView);
    }

    /**
     * testGalleryBuilderFrom
     */
    @Test
    public void testGalleryBuilderFrom() {
        Context context = AbilityDelegatorRegistry.getAbilityDelegator().getCurrentTopAbility().getContext();
        ScrollGalleryView scrollGalleryView = new ScrollGalleryView(context,null);
        GalleryBuilder galleryBuilder = ScrollGalleryView.from(scrollGalleryView);
        assertNotNull(galleryBuilder);
    }
}