/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.veinhorn.scrollgalleryview.util;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import java.util.Optional;

/**
 * 类似 BitmapFactory.decodeResource 获取 Bitmap
 * ImageSource.DecodingOptions可以自己按需求添加其他属性
 *
 * @author: 252232-renpengfei
 * @since 2021-03-18
 */
public final class FactoryUtil {
    private FactoryUtil() {
    }

    /**
     * 获取屏幕信息
     *
     * @param context
     * @return DisplayAttributes
     */
    public static DisplayAttributes getOptionalInfo(Context context) {
        Optional<Display> display = DisplayManager.getInstance().getDefaultDisplay(context);
        DisplayAttributes displayAttributes = display.get().getAttributes();
        return displayAttributes;
    }
}
