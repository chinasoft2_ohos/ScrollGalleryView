package com.veinhorn.scrollgalleryview;

import com.veinhorn.playerviewlibrary.HmPlayer;
import com.veinhorn.playerviewlibrary.player.api.ImplPlayer;
import com.veinhorn.playerviewlibrary.player.view.PlayerLoading;
import com.veinhorn.playerviewlibrary.player.view.PlayerView;
import com.veinhorn.playerviewlibrary.player.view.SimplePlayerController;
import com.veinhorn.playerviewlibrary.util.ScreenUtils;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DependentLayout;
import ohos.agp.window.service.WindowManager;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.bundle.AbilityInfo;

/**
 * VideoPlayerAbility
 *
 * @since 2021-03-24
 */
public class VideoPlayerAbility extends Ability {
    private ImplPlayer player;
    private DependentLayout parentLayout;
    private PlayerView playerView;
    private PlayerLoading loadingView;
    private SimplePlayerController controllerView;

    /**
     * onStart
     *
     * @param intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_video_fragment);
        this.getWindow().addFlags(WindowManager.LayoutConfig.MARK_FULL_SCREEN);

        String url = intent.getStringParam(Constants.STR_VIDEOURL);

        player = new HmPlayer.Builder(this).setFilePath(url).create();
        player.getLifecycle().onStart();
        initComponent();
    }

    private void initComponent() {
        if (findComponentById(ResourceTable.Id_parent) instanceof DependentLayout) {
            parentLayout = (DependentLayout) findComponentById(ResourceTable.Id_parent);
        }
        if (findComponentById(ResourceTable.Id_player_view) instanceof PlayerView) {
            playerView = (PlayerView) findComponentById(ResourceTable.Id_player_view);
        }
        if (findComponentById(ResourceTable.Id_loading_view) instanceof PlayerLoading) {
            loadingView = (PlayerLoading) findComponentById(ResourceTable.Id_loading_view);
        }
        if (findComponentById(ResourceTable.Id_controller_view) instanceof SimplePlayerController) {
            controllerView = (SimplePlayerController) findComponentById(ResourceTable.Id_controller_view);
        }
        playerView.bind(player);
        loadingView.bind(player);
        controllerView.bind(player);
    }

    /**
     * onOrientationChanged
     *
     * @param displayOrientation
     */
    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        int screenWidth = ScreenUtils.getScreenWidth(this);
        parentLayout.setWidth(screenWidth);
        player.openGesture(displayOrientation == AbilityInfo.DisplayOrientation.LANDSCAPE);
    }

    /**
     * onActive
     */
    @Override
    protected void onActive() {
        super.onActive();
        getGlobalTaskDispatcher(TaskPriority.DEFAULT).delayDispatch(() -> player.play(), Constants.INT_NUMBER_1000);
    }

    /**
     * onForeground
     *
     * @param intent
     */
    @Override
    public void onForeground(Intent intent) {
        player.getLifecycle().onForeground();
        super.onForeground(intent);
    }

    /**
     * onBackground
     */
    @Override
    protected void onBackground() {
        player.getLifecycle().onBackground();
        super.onBackground();
    }

    /**
     * onStop
     */
    @Override
    protected void onStop() {
        loadingView.unbind();
        controllerView.unbind();
        playerView.unbind();
        player.getLifecycle().onStop();
        super.onStop();
    }
}
