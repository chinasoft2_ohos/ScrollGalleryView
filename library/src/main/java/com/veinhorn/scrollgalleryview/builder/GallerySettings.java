package com.veinhorn.scrollgalleryview.builder;

import ohos.aafwk.ability.fraction.FractionManager;

/**
 * GallerySettings
 *
 * @since 2021-05-13
 */
public class GallerySettings {
    private int mThumbnailSize;
    private boolean mIsZoomEnabled;
    private FractionManager mFragmentManager;

    /**
     * getThumbnailSize
     *
     * @return int
     */
    public int getThumbnailSize() {
        return mThumbnailSize;
    }

    /**
     * setThumbnailSize
     *
     * @param thumbnailSize
     */
    public void setThumbnailSize(int thumbnailSize) {
        this.mThumbnailSize = thumbnailSize;
    }

    /**
     * isZoomEnabled
     *
     * @return boolean
     */
    public boolean isZoomEnabled() {
        return mIsZoomEnabled;
    }

    /**
     * setZoomEnabled
     *
     * @param isZoomEnabled
     */
    public void setZoomEnabled(boolean isZoomEnabled) {
        mIsZoomEnabled = isZoomEnabled;
    }

    /**
     * getFragmentManager
     *
     * @return FractionManager
     */
    public FractionManager getFragmentManager() {
        return mFragmentManager;
    }

    /**
     * setFragmentManager
     *
     * @param fragmentManager
     */
    public void setFragmentManager(FractionManager fragmentManager) {
        this.mFragmentManager = fragmentManager;
    }

    /**
     * from
     *
     * @param fm
     * @return GallerySettingsBuilder
     */
    public static GallerySettingsBuilder from(FractionManager fm) {
        GallerySettingsBuilder builder = new GallerySettingsBuilderImpl();
        builder.withFragmentManager(fm);
        return builder;
    }
}

