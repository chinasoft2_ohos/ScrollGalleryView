package ogbe.ozioma.com.glideimageloader;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.veinhorn.scrollgalleryview.ResourceTable;
import com.veinhorn.scrollgalleryview.loader.MediaLoader;
import com.veinhorn.scrollgalleryview.util.LogUtil;
import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.app.Context;
import org.jetbrains.annotations.Nullable;

/**
 * GlideImageLoader
 *
 * @since 2021-05-13
 */
public class GlideImageLoader implements MediaLoader {
    private String mUrl;
//    private int mWidth;
//    private int mHeight;

    /**
     * GlideImageLoader
     *
     * @param url String
     */
    public GlideImageLoader(String url) {
        this.mUrl = url;
    }

    /**
     * GlideImageLoader
     *
     * @param url
     * @param width
     * @param height
     */
    public GlideImageLoader(String url, int width, int height) {
        this.mUrl = url;
//        this.mWidth = width;
//        this.mHeight = height;
    }

    /**
     * isImage
     *
     * @return boolean
     */
    @Override
    public boolean isImage() {
        return true;
    }

    /**
     * loadMedia
     *
     * @param context
     * @param imageView
     * @param callback
     */
    @Override
    public void loadMedia(Context context, Image imageView, final SuccessCallback callback) {
        RequestOptions requestOptions = new RequestOptions();
        Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(mUrl)
                .skipMemoryCache(false)
//                .placeholder(ResourceTable.Media_placeholder_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new RequestListener<Element>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object o,
                                                Target<Element> target, boolean isBoolean) {
                        LogUtil.error("onLoadFailed","加载失败");
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Element element, Object o,
                                                   Target<Element> target, DataSource dataSource, boolean isBoolean) {
                        callback.onSuccess();
                        return false;
                    }
                })
                .into(imageView);
    }

    /**
     * loadThumbnail
     *
     * @param context
     * @param thumbnailView
     * @param callback
     */
    @Override
    public void loadThumbnail(Context context, Image thumbnailView, final SuccessCallback callback) {
        Glide.with(context)
                .load(mUrl)
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .listener(new RequestListener<Element>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e,
                                                Object model,
                                                Target<Element> target,
                                                boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Element resource,
                                                   Object model,
                                                   Target<Element> target,
                                                   DataSource dataSource,
                                                   boolean isFirstResource) {
                        callback.onSuccess();
                        return false;
                    }
                })
                .into(thumbnailView);
    }
}
