package com.veinhorn.scrollgalleryview;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * ScreenSlidePagerAdapter
 *
 * @author veinhorn
 * @since 2021-03-24
 */
public class ScreenSlidePagerAdapter extends PageSliderProvider {
    private List<MediaInfo> mListOfMedia;
    private boolean mIsZoom = false;
    private ScrollGalleryView.OnImageClickListener mOnImageClickListener;
    private ScrollGalleryView.OnImageLongClickListener mOnImageLongClickListener;
    private Context mContext;
    private List<ImageFragment> imageFragmentList;

    /**
     * ScreenSlidePagerAdapter
     *
     * @param context
     * @param listOfMedia
     * @param isZoom
     * @param onImageClickListener
     * @param onImageLongClickListener
     */
    public ScreenSlidePagerAdapter(Context context, List<MediaInfo> listOfMedia,
                                   boolean isZoom, ScrollGalleryView.OnImageClickListener onImageClickListener,
                                   ScrollGalleryView.OnImageLongClickListener onImageLongClickListener) {
        this.mListOfMedia = listOfMedia;
        this.mIsZoom = isZoom;
        this.mOnImageClickListener = onImageClickListener;
        this.mContext = context;
        this.mOnImageLongClickListener = onImageLongClickListener;
//        imageFragmentList = new ArrayList<>();
        imageFragmentList = getImageComponent();
    }

    /**
     * getCount
     *
     * @return int
     */
    @Override
    public int getCount() {
        return imageFragmentList.size();
    }

    /**
     * createPageInContainer
     *
     * @param componentContainer
     * @param i
     * @return Object
     */
    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        int count = componentContainer.getChildCount();
        if (count != 0) {
            for (int index = 0; index < count; index++) {
                Component component = componentContainer.getComponentAt(index);
                ImageFragment imageFragment = imageFragmentList.get(i);
                if (imageFragment.getTag() != component.getTag()) {
                    imageFragment.setTag(i);
                    componentContainer.addComponent(imageFragment);
                }
            }
        } else {
            ImageFragment imageFragment = imageFragmentList.get(i);
            imageFragment.setTag(i);
            componentContainer.addComponent(imageFragment);
        }

        return imageFragmentList.get(i);
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
    }

    /**
     * isPageMatchToObject
     *
     * @param component
     * @param object
     * @return boolean
     */
    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }

    private List<ImageFragment> getImageComponent() {
        List<ImageFragment> mImageFragmentList = new ArrayList<>();
        for (int index = 0; index < mListOfMedia.size(); index++) {
            ImageFragment imageFragment = new ImageFragment(mContext, mListOfMedia.get(index), mIsZoom);
            int finalIndex = index;
            imageFragment.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component component) {
                    mOnImageClickListener.onClick(finalIndex);
                }
            });
            imageFragment.setLongClickedListener(new Component.LongClickedListener() {
                @Override
                public void onLongClicked(Component component) {
                    mOnImageLongClickListener.onClick(finalIndex);
                }
            });
            mImageFragmentList.add(imageFragment);
        }
        return mImageFragmentList;
    }
}
