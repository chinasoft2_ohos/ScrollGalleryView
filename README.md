# ScrollGalleryView

#### 项目介绍
- 项目名称：ScrollGalleryView
- 所属系列：openharmony的第三方组件适配移植
- 功能：ScrollGalleryView是一个灵活的库，可帮助您在应用程序中创建很棒的媒体库。它可以轻松地与最流行的图像加载库（例如Picasso，Glide和Fresco）集成。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release v1.2.7

#### 效果演示

<img src="gif/videoView.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
```java
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
```java
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ScrollGalleryView-library:1.0.2')
    ......
 }
```

在sdk6，DevEco Studio 2.2 Beta1 下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

3.图片加载器有三种实现Picasso、Glide、Fresco
    
```xml
Glide
        implementation('com.gitee.chinasoft_ohos:ScrollGalleryView-glide:1.0.2')
        
Picasso
       implementation('com.gitee.chinasoft_ohos:ScrollGalleryView-picasso:1.0.2')
        
Fresco
        implementation('com.gitee.chinasoft_ohos:ScrollGalleryView-fresco:1.0.2')
        
```

#### 使用说明

1.在xml中添加ScrollGalleryView控件

```xml
    <DirectionalLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:orientation="vertical">
    
        <com.veinhorn.scrollgalleryview.ScrollGalleryView
            ohos:id="$+id:scroll_gallery_view"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:background_element="#000000"/>
    </DirectionalLayout>
```

2.初始化:

```java
ScrollGalleryView scrollGalleryView = (ScrollGalleryView) findComponentById(ResourceTable.Id_scroll_gallery_view);
        ScrollGalleryView
                .from(scrollGalleryView)
                .settings(
                        GallerySettings
                                .from(getFractionManager())
                                .thumbnailSize(mThumbnailSize)
                                .enableZoom(true)
                                .build()
                )
                .onImageClickListener(new ScrollGalleryView.OnImageClickListener() {
                    @Override
                    public void onClick(int position) {
                    }
                })
                .onImageLongClickListener(new ScrollGalleryView.OnImageLongClickListener() {
                    @Override
                    public void onClick(int position) {
                    }
                })
                .add(images(
                        "https://img0.baidu.com/it/u=3438413553,834879777&fm=26&fmt=auto&gp=0.jpg",
                        "https://img2.baidu.com/it/u=3355464299,584008140&fm=26&fmt=auto&gp=0.jpg",
                        "https://img1.baidu.com/it/u=254528806,950894871&fm=26&fmt=auto&gp=0.jpg",
                        "https://img0.baidu.com/it/u=3438413553,834879777&fm=26&fmt=auto&gp=0.jpg",
                        "https://img2.baidu.com/it/u=3355464299,584008140&fm=26&fmt=auto&gp=0.jpg",
                        "https://img1.baidu.com/it/u=254528806,950894871&fm=26&fmt=auto&gp=0.jpg"
                ))
                .add(video("https://www.w3school.com.cn/example/html5/mov_bbb.mp4", ResourceTable.Media_default_video))
                .build();
```
ScrollGalleryView支持不同类型的媒体，例如图像和视频。您可以创建图片和视频集合来添加它们。

3.接口说明
```java
   /**
     * 缩略图大小
     *
     * @param thumbnailSize
     */
    void setThumbnailSize(int thumbnailSize)
    
    
   /**
     * 是否启用缩放
     *
     * @param isZoomEnabled
     * @return ScrollGalleryView
     */
    ScrollGalleryView setZoom(boolean isZoomEnabled) {
    
    
   /**
     * 单击回调
     *
     * @param listener ScrollGalleryView.OnImageClickListener
     * @return GalleryBuilder
     */
    GalleryBuilder onImageClickListener(ScrollGalleryView.OnImageClickListener listener)
    
    
   /**
     * 长按回调
     *
     * @param listener
     * @return ScrollGalleryView.OnImageLongClickListener
     */
    GalleryBuilder onImageLongClickListener(ScrollGalleryView.OnImageLongClickListener listener);
    
    
   /**
     * 隐藏缩略图 
     *
     * @param thumbnailSize
     */
    ScrollGalleryView withHiddenThumbnails(boolean isThumbnailsHidden)
    
   /**
     * 点击隐藏缩略图
     *
     * @param isHideThumbnailsOnClick hides thumbnails container on image click
     * @return ScrollGalleryView
     */
    ScrollGalleryView hideThumbnailsOnClick(boolean isHideThumbnailsOnClick) {
    
   /**
     * 添加资源
     *
     * @param medias
     * @return GalleryBuilder
     */
    GalleryBuilder add(List<MediaInfo> medias)
```


## 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

## 版本迭代

- 1.0.2

## 版权和许可信息
MIT License

Copyright (c) 2019 Boris Korogvich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.