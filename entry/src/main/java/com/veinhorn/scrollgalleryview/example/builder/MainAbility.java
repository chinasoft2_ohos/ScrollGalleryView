/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.veinhorn.scrollgalleryview.example.builder;

import com.github.ybq.core.style.Wave;
import com.veinhorn.scrollgalleryview.ScrollGalleryView;
import com.veinhorn.scrollgalleryview.builder.GallerySettings;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.utils.Color;
import ohos.utils.PacMap;

import static ogbe.ozioma.com.glideimageloader.dsl.DSL.images;
import static ogbe.ozioma.com.glideimageloader.dsl.DSL.video;

/**
 * MainAbility
 *
 * @since 2021-05-13
 */
public class MainAbility extends Ability {
    private static final int THUMBNAILSIZE = 100;
    private static final int RUNNABLETIME = 2500;
    private static final int LOADINGSIZE = 300;
    private ScrollGalleryView scrollGalleryView;
    private int mPageCurrent = 0;
    private Button mView;

    /**
     * onStart
     *
     * @param intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        scrollGalleryView = (ScrollGalleryView) findComponentById(ResourceTable.Id_scroll_gallery_view);
        mView = (Button) findComponentById(ResourceTable.Id_m_view);
        initLoadingView();
        getUITaskDispatcher()
                .delayDispatch(
                        new Runnable() {
                            @Override
                            public void run() {
                                mView.setVisibility(Component.HIDE);
                                scrollGalleryView.setVisibility(Component.VISIBLE);
                            }
                        },RUNNABLETIME);
        initScrollGalleryView();
    }

    private void initLoadingView() {
        Wave wave = new Wave();
        wave.setBounds(0, 0, LOADINGSIZE, LOADINGSIZE);
        wave.setComponent(mView);
        wave.setPaintColor(Color.getIntColor("#d71345"));
        mView.addDrawTask((component, canvas) -> wave.drawToCanvas(canvas));
    }

    private void initScrollGalleryView() {
        ScrollGalleryView
                .from(scrollGalleryView)
                .settings(
                        GallerySettings
                                .from(getFractionManager())
                                .thumbnailSize(THUMBNAILSIZE)
                                .enableZoom(true)
                                .build()
                )
                .onImageClickListener(new ScrollGalleryView.OnImageClickListener() {
                    @Override
                    public void onClick(int position) {
                    }
                })
                .onImageLongClickListener(new ScrollGalleryView.OnImageLongClickListener() {
                    @Override
                    public void onClick(int position) {
                    }
                })
                .add(images(getString(ResourceTable.String_photoUrl1),
                        getString(ResourceTable.String_photoUrl2),
                        getString(ResourceTable.String_photoUrl3),
                        getString(ResourceTable.String_photoUrl4)
                ))
                .add(video("entry/resources/base/media/gubeishuizhen.mp4", ResourceTable.Media_default_video))
                .build();

        scrollGalleryView.getViewPager().addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int pageCurrent, float v, int i1) {
            }

            @Override
            public void onPageSlideStateChanged(int pageCurrent) {
            }

            @Override
            public void onPageChosen(int pageCurrent) {
                mPageCurrent = pageCurrent;
            }
        });
    }

    /**
     * onSaveAbilityState
     *
     * @param outState
     */
    @Override
    public void onSaveAbilityState(PacMap outState) {
        super.onSaveAbilityState(outState);
        outState.putIntValue("abilityState", mPageCurrent);
    }

    /**
     * onRestoreAbilityState
     *
     * @param inState
     */
    @Override
    public void onRestoreAbilityState(PacMap inState) {
        super.onRestoreAbilityState(inState);

        if (inState != null) {
            int pageCurrent = inState.getIntValue("abilityState");
            scrollGalleryView.setCurrentItem(pageCurrent);
        }
    }

    /**
     * getFractionManager
     *
     * @return FractionManager
     */
    private FractionManager getFractionManager() {
        Ability ability = this;
        FractionManager fractionManager = null;
        if (ability instanceof FractionAbility) {
            FractionAbility fractionAbility = (FractionAbility) ability;
            fractionManager = fractionAbility.getFractionManager();
            return fractionManager;
        }
        return fractionManager;
    }
}
