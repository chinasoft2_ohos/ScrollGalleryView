package com.veinhorn.scrollgalleryview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.PageSlider;
import ohos.app.Context;

/**
 * HackyViewPager
 *
 * @author Chris Banes
 * @since 2021-03-24
 */
public class HackyViewPager extends PageSlider {
    /**
     * HackyViewPager
     *
     * @param context
     */
    public HackyViewPager(Context context) {
        super(context, null);
    }

    /**
     * HackyViewPager
     *
     * @param context
     * @param attrSet
     */
    public HackyViewPager(Context context, AttrSet attrSet) {
        super(context, attrSet, null);
    }

    /**
     * HackyViewPager
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public HackyViewPager(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
