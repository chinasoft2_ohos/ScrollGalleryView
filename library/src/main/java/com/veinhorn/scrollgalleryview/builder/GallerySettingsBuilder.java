package com.veinhorn.scrollgalleryview.builder;

import ohos.aafwk.ability.fraction.FractionManager;

/**
 * GallerySettingsBuilder
 *
 * @since 2021-05-13
 */
public interface GallerySettingsBuilder {
    /**
     * thumbnailSize
     *
     * @param thumbnailSize
     * @return GallerySettingsBuilder
     */
    GallerySettingsBuilder thumbnailSize(int thumbnailSize);

    /**
     * enableZoom
     *
     * @param isZoomEnabled
     * @return GallerySettingsBuilder
     */
    GallerySettingsBuilder enableZoom(boolean isZoomEnabled);

    /**
     * withFragmentManager
     *
     * @param fragmentManager
     * @return GallerySettingsBuilder
     */
    GallerySettingsBuilder withFragmentManager(FractionManager fragmentManager);

    /**
     * build
     *
     * @return GallerySettings
     */
    GallerySettings build();
}
