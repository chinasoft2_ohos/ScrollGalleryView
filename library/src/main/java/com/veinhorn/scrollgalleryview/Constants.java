package com.veinhorn.scrollgalleryview;

/**
 * LogUtil工具类
 *
 * @author Alexey Nevinsky
 * @since 2021-03-24
 */
public final class Constants {
    /**
     * ZOOM
     */
    public static final String ZOOM = "zoom";

    /**
     * IS_VIDEO
     */
    public static final String IS_VIDEO = "isVideo";

    /**
     * URL
     */
    public static final String URL = "url";

    /**
     * IS_LOCKED
     */
    public static final String IS_LOCKED = "isLocked";

    /**
     * IMAGE
     */
    public static final String IMAGE = "image";

    /**
     * POSITION
     */
    public static final String POSITION = "position";

    /**
     * 跳转标识
     */
    public static final int START_ABILITY = 101;

    /**
     * 视频播放地址
     */
    public static final String STR_VIDEOURL = "videoUrl";

    /**
     * INT_NUMBER_1000
     */
    public static final int INT_NUMBER_1000 = 1000;

    private Constants() {
    }
}
