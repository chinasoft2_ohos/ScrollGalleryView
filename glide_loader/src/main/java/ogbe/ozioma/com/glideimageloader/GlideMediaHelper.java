package ogbe.ozioma.com.glideimageloader;

import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.builder.BasicMediaHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * GlideMediaHelper
 *
 * @since 2021-05-13
 */
public class GlideMediaHelper extends BasicMediaHelper {
    /**
     * image
     *
     * @param url is an image URL address
     * @return MediaInfo
     */
    @Override
    public MediaInfo image(String url) {
        return MediaInfo.mediaLoader(new GlideImageLoader(url));
    }

    /**
     * images
     *
     * @param urls List
     * @return List
     */
    @Override
    public List<MediaInfo> images(List<String> urls) {
        List<MediaInfo> medias = new ArrayList<>();

        for (String url : urls) {
            medias.add(mediaInfo(url));
        }

        return medias;
    }

    /**
     * images
     *
     * @param urls String[]
     * @return MediaInfo
     */
    @Override
    public List<MediaInfo> images(String... urls) {
        return images(Arrays.asList(urls));
    }

    /**
     * mediaInfo
     *
     * @param url
     * @return MediaInfo
     */
    private MediaInfo mediaInfo(String url) {
        return MediaInfo.mediaLoader(new GlideImageLoader(url));
    }
}
