package com.ayokunlepaul.frescoloader;

import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.builder.BasicMediaHelper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * FrescoMediaHelper
 *
 * @since 2021-05-13
 */
public class FrescoMediaHelper extends BasicMediaHelper {
    /**
     * image
     *
     * @param url is an image URL address
     * @return MediaInfo
     */
    @Override
    public MediaInfo image(String url) {
        return MediaInfo.mediaLoader(new FrescoImageLoader(url));
    }

    /**
     * images
     *
     * @param urls is an list of image urls
     * @return List
     */
    @Override
    public List<MediaInfo> images(List<String> urls) {
        List<MediaInfo> medias = new ArrayList<>();

        for (String url : urls) {
            medias.add(mediaInfo(url));
        }

        return medias;
    }

    /**
     * images
     *
     * @param urls is an array of image urls
     * @return List
     */
    @Override
    public List<MediaInfo> images(String... urls) {
        return images(Arrays.asList(urls));
    }

    private MediaInfo mediaInfo(String url) {
        return MediaInfo.mediaLoader(new FrescoImageLoader(url));
    }
}
