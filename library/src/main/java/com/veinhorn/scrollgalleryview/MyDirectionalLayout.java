package com.veinhorn.scrollgalleryview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

/**
 * MyDirectionalLayout
 *
 * @since 2021-03-24
 */
public class MyDirectionalLayout extends DirectionalLayout implements Component.TouchEventListener {
    /**
     * MyDirectionalLayout
     *
     * @param context
     */
    public MyDirectionalLayout(Context context) {
        this(context, null);
    }

    /**
     * MyDirectionalLayout
     *
     * @param context
     * @param attrSet
     */
    public MyDirectionalLayout(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * MyDirectionalLayout
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public MyDirectionalLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setTouchEventListener(this);
    }

    /**
     * onTouchEvent
     *
     * @param component
     * @param touchEvent
     * @return boolean
     */
    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        return true;
    }
}
