## 1.0.2
正式版本
## 0.0.2-SNAPSHOT
ohos 第二个版本
 * 适配SKD 6
## 0.0.1-SNAPSHOT
ohos 第一个版本
 * 实现了原库的大部分 api
 * 因为 图片展示使用外部依赖photoView，其放大图片时左右移动图片查看会切换至下一张（图片放大后的水平部分无法查看）
 * 视频播放原有组件直接调用的是系统的播放器，现组件使用SimpleVideoCodelab，视频播界面会和原有组件有所差异