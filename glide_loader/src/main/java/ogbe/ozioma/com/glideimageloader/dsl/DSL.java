package ogbe.ozioma.com.glideimageloader.dsl;

import com.veinhorn.scrollgalleryview.MediaInfo;
import ogbe.ozioma.com.glideimageloader.GlideMediaHelper;

import java.util.List;

/**
 * DSL
 *
 * @since 2021-05-13
 */
public final class DSL {
    private static GlideMediaHelper mediaHelper = new GlideMediaHelper();

    private DSL() {
    }

    /**
     * image
     *
     * @param url
     * @return MediaInfo
     */
    public static MediaInfo image(String url) {
        return mediaHelper.image(url);
    }

    /**
     * images
     *
     * @param urls
     * @return List
     */
    public static List<MediaInfo> images(List<String> urls) {
        return mediaHelper.images(urls);
    }

    /**
     * images
     *
     * @param urls
     * @return List
     */
    public static List<MediaInfo> images(String... urls) {
        return mediaHelper.images(urls);
    }

    /**
     * MediaInfo
     *
     * @param url String
     * @param placeholderViewId int
     * @return MediaInfo
     */
    public static MediaInfo video(String url, int placeholderViewId) {
        return mediaHelper.video(url, placeholderViewId);
    }
}

