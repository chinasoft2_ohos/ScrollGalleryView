package com.veinhorn.scrollgalleryview.builder;

import com.veinhorn.scrollgalleryview.MediaInfo;

import java.util.List;

/**
 * MediaHelper
 *
 * @since 2021-05-13
 */
public interface MediaHelper {
    /**
     * Creates MediaInfo object based on image url
     *
     * @param url is an image URL address
     * @return MediaInfo object
     */
    MediaInfo image(String url);

    /**
     * Creates an list of MediaInfos from the list of image urls
     *
     * @param urls is an list of image urls
     * @return an list of MediaInfo objects
     */
    List<MediaInfo> images(List<String> urls);

    /**
     * Creates an list of MediaInfos from the array of image urls
     *
     * @param urls is an array of image urls
     * @return an list of MediaInfo objects
     */
    List<MediaInfo> images(String... urls);

    /**
     * Creates MediaInfo object based on image url
     *
     * @param url is an image URL address
     * @param placeholderViewId is an image resource id which is used as video placeholder
     * @return MediaInfo object
     */
    MediaInfo video(String url, int placeholderViewId);
}
