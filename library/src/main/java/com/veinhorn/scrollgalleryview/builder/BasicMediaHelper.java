package com.veinhorn.scrollgalleryview.builder;

import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.loader.DefaultVideoLoader;

/**
 * BasicMediaHelper
 *
 * @since 2021-05-13
 */
public abstract class BasicMediaHelper implements MediaHelper {
    /**
     * video
     *
     * @param url String
     * @param placeholderViewId int
     * @return MediaInfo
     */
    @Override
    public MediaInfo video(String url, int placeholderViewId) {
        return MediaInfo.mediaLoader(new DefaultVideoLoader(url, placeholderViewId));
    }
}
