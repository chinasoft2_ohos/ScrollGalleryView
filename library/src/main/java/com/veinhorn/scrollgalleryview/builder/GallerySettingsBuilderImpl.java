package com.veinhorn.scrollgalleryview.builder;

import ohos.aafwk.ability.fraction.FractionManager;

/**
 * GallerySettingsBuilderImpl
 *
 * @since 2021-05-13
 */
public class GallerySettingsBuilderImpl implements GallerySettingsBuilder {
    private GallerySettings gallerySettings;

    /**
     * GallerySettingsBuilderImpl
     */
    public GallerySettingsBuilderImpl() {
        this.gallerySettings = new GallerySettings();
    }

    /**
     * thumbnailSize
     *
     * @param thumbnailSize
     * @return GallerySettingsBuilder
     */
    @Override
    public GallerySettingsBuilder thumbnailSize(int thumbnailSize) {
        gallerySettings.setThumbnailSize(thumbnailSize);
        return this;
    }

    /**
     * enableZoom
     *
     * @param isZoomEnabled
     * @return GallerySettingsBuilder
     */
    @Override
    public GallerySettingsBuilder enableZoom(boolean isZoomEnabled) {
        gallerySettings.setZoomEnabled(isZoomEnabled);
        return this;
    }

    /**
     * withFragmentManager
     *
     * @param fragmentManager
     * @return GallerySettingsBuilder
     */
    @Override
    public GallerySettingsBuilder withFragmentManager(FractionManager fragmentManager) {
        gallerySettings.setFragmentManager(fragmentManager);
        return this;
    }

    /**
     * build
     *
     * @return GallerySettings
     */
    @Override
    public GallerySettings build() {
        return gallerySettings;
    }
}
