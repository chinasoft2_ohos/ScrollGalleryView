package com.veinhorn.scrollgalleryview.builder;

import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.ScrollGalleryView;

import java.util.List;

/**
 * GalleryBuilder
 *
 * @since 2021-05-13
 */
public interface GalleryBuilder {
    /**
     * Sets up settings for gallery
     *
     * @param settings contains options for gallery
     * @return GalleryBuilder object
     */
    GalleryBuilder settings(GallerySettings settings);

    /**
     * Sets up settings for gallery
     *
     * @param listener ScrollGalleryView.OnImageClickListener
     * @return GalleryBuilder
     */
    GalleryBuilder onImageClickListener(ScrollGalleryView.OnImageClickListener listener);

    /**
     * onImageLongClickListener
     *
     * @param listener
     * @return ScrollGalleryView.OnImageLongClickListener
     */
    GalleryBuilder onImageLongClickListener(ScrollGalleryView.OnImageLongClickListener listener);

    /**
     * Adds single MediaInfo to gallery
     *
     * @param media MediaInfo
     * @return GalleryBuilder
     */
    GalleryBuilder add(MediaInfo media);

    /**
     * add
     *
     * @param medias List
     * @return GalleryBuilder
     */
    GalleryBuilder add(List<MediaInfo> medias);

    /**
     * Builds gallery from provided medias
     *
     * @return initialized ScrollGalleryView
     */
    ScrollGalleryView build();
}

