package com.ayokunlepaul.frescoloader;

import com.facebook.common.executors.UiThreadImmediateExecutorService;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.datasource.DataSubscriber;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.common.ResizeOptions;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;
import com.veinhorn.scrollgalleryview.loader.MediaLoader;
import com.veinhorn.scrollgalleryview.util.LogUtil;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.utils.net.Uri;

/**
 * FrescoImageLoader
 *
 * @author ayokunlepaul
 * @since 2021-05-13
 */
public class FrescoImageLoader implements MediaLoader {
    private String mUrl;
    private int mThumbnailWidth = 1;
    private int mThumbnailHeight = 1;

    /**
     * FrescoImageLoader
     *
     * @param url
     */
    public FrescoImageLoader(String url) {
        this.mUrl = url;
    }

    /**
     * FrescoImageLoader
     *
     * @param url
     * @param thumbnailWidth
     * @param thumbnailHeight
     */
    public FrescoImageLoader(String url, int thumbnailWidth, int thumbnailHeight) {
        this.mUrl = url;
        this.mThumbnailWidth = thumbnailWidth;
        this.mThumbnailHeight = thumbnailHeight;
    }

    /**
     * isImage
     *
     * @return boolean
     */
    @Override
    public boolean isImage() {
        return true;
    }

    /**
     * loadMedia
     *
     * @param context
     * @param imageView
     * @param callback
     */
    @Override
    public void loadMedia(Context context, final Image imageView, SuccessCallback callback) {
        if (!Fresco.hasBeenInitialized()) {
            Fresco.initialize(context);
        }
        ImagePipeline pipeline = Fresco.getImagePipeline();
        DataSubscriber subscriber = getSubscriber(imageView, callback);
        DataSource<CloseableReference<CloseableImage>> dataSource = pipeline
                .fetchDecodedImage(createImageRequest(), context);
        dataSource.subscribe(subscriber, UiThreadImmediateExecutorService.getInstance());
    }

    /**
     * loadThumbnail
     *
     * @param context
     * @param thumbnailView
     * @param callback
     */
    @Override
    public void loadThumbnail(Context context, Image thumbnailView, SuccessCallback callback) {
        if (!Fresco.hasBeenInitialized()) {
            Fresco.initialize(context);
        }
        ImagePipeline pipeline = Fresco.getImagePipeline();
        DataSubscriber subscriber = getSubscriber(thumbnailView, callback);
        DataSource<CloseableReference<CloseableImage>> dataSource = pipeline
                .fetchDecodedImage(createImageRequest(mThumbnailWidth, mThumbnailHeight), context);
        dataSource.subscribe(subscriber, UiThreadImmediateExecutorService.getInstance());
    }

    private ImageRequest createImageRequest() {
        LogUtil.error("createImageRequest", "url=" + mUrl);
        return ImageRequest.fromUri(Uri.parse(mUrl));
    }

    private ImageRequest createImageRequest(int thumbnailWidth, int thumbnailHeight) {
        ImageRequestBuilder builder = ImageRequestBuilder.newBuilderWithSource(Uri.parse(mUrl));
        builder.setResizeOptions(new ResizeOptions(thumbnailWidth, thumbnailHeight));

        return builder.build();
    }

    private DataSubscriber getSubscriber(final Image imageView, final SuccessCallback callback) {
        return new DataSubscriberAchieve(imageView, callback);
    }
}
