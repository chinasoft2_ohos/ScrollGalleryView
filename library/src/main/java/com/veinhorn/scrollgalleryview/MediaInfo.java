package com.veinhorn.scrollgalleryview;

import com.veinhorn.scrollgalleryview.loader.MediaLoader;

/**
 * MediaInfo
 *
 * @since 2021-03-24
 */
public class MediaInfo {
    private MediaLoader mLoader;

    /**
     * mediaLoader
     *
     * @param mediaLoader
     * @return MediaInfo
     */
    public static MediaInfo mediaLoader(MediaLoader mediaLoader) {
        return new MediaInfo().setLoader(mediaLoader);
    }

    /**
     * getLoader
     *
     * @return MediaLoader
     */
    public MediaLoader getLoader() {
        return mLoader;
    }

    /**
     * setLoader
     *
     * @param loader
     * @return MediaInfo
     */
    public MediaInfo setLoader(MediaLoader loader) {
        mLoader = loader;
        return this;
    }
}

