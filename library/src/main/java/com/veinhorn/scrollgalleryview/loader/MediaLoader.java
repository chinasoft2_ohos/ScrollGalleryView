package com.veinhorn.scrollgalleryview.loader;

import ohos.agp.components.Image;
import ohos.app.Context;

/**
 * MediaLoader
 *
 * @since 2021-05-13
 */
public interface MediaLoader {
    /**
     * isImage
     *
     * @return true if implementation load's image, otherwise false
     */
    boolean isImage();

    /**
     * Loads image and sets it to imageView. After that implementation can call callback to set imageView's
     * scale type to ScaleType.FIT_CENTER.
     *
     * @param context
     * @param imageView
     * @param callback
     */
    void loadMedia(Context context, Image imageView, SuccessCallback callback);

    /**
     * Loads thumbnail image and sets it to thumbnailView. After that implementation can call callback
     * to set thumbnailView's scale type to ScaleType.FIT_CENTER.
     *
     * @param context
     * @param thumbnailView
     * @param callback
     */
    void loadThumbnail(Context context, Image thumbnailView, SuccessCallback callback);

    /**
     * Implementation may call this callback for report to imageView, what it's image was changed
     *
     * @since 2021-05-13
     */
    interface SuccessCallback {
        void onSuccess();
    }
}
