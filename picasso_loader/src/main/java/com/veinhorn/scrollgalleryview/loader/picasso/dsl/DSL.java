package com.veinhorn.scrollgalleryview.loader.picasso.dsl;

import com.veinhorn.scrollgalleryview.MediaInfo;
import com.veinhorn.scrollgalleryview.loader.picasso.PicassoMediaHelper;

import java.util.List;

/**
 * DSL
 *
 * @since 2021-05-13
 */
public final class DSL {
    private static PicassoMediaHelper mediaHelper = new PicassoMediaHelper();

    private DSL() {
    }

    /**
     * image
     *
     * @param url
     * @return MediaInfo
     */
    public static MediaInfo image(String url) {
        return mediaHelper.image(url);
    }

    /**
     * images
     *
     * @param urls
     * @return List
     */
    public static List<MediaInfo> images(List<String> urls) {
        return mediaHelper.images(urls);
    }

    /**
     * images
     *
     * @param urls
     * @return List
     */
    public static List<MediaInfo> images(String... urls) {
        return mediaHelper.images(urls);
    }

    /**
     * video
     *
     * @param url
     * @param placeholderViewId
     * @return MediaInfo
     */
    public static MediaInfo video(String url, int placeholderViewId) {
        return mediaHelper.video(url, placeholderViewId);
    }
}

